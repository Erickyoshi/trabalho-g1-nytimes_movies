import movieService from "../../service/moviess/MoviesService"


const CriticsPage = () => {
    const [critics, setCritics] = useState([])
    const [listCritics, setListCritics] = useState('')

    const { reviewer } = useParams()

    useEffect(() => {

        const loadCritics = async () => {
            let result = await movieService.getListMovieCritics(reviewer)
            
            setCritics(result.critics)
            setListCritics(result.listCritics)
        }

        loadCritics()
    }, [ reviewer ])

    return (
        <>
            <Typography variant="h5">
                {listCritics}
            </Typography>
            <Grid container spacing={2} marginTop={2}>
                {
                    critics.map(critics => 
                        <CriticsItem key={critics.display_name} critics={critics} />
                    )
                }
            </Grid>
        </>
    )
}

export default CriticsPage