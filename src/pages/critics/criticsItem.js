import { Grid, Paper, Stack, Typography } from '@mui/material'

const CriticsItem = ({ critic }) => {
    return (
        <Grid item md={4} sm={6} xs={12}>
            <Stack padding={2} borderRadius={2} component={Paper} height={350}>
                <img src={critic.multimedia} style={{ objectFit: 'cover', height: "200px" }} />

                <Typography fontWeight="bold" variant="subtitle1">
                    {critic.display_name}. {critic.sort_name}
                </Typography>
                <Typography variant="body2">
                    {critic.seo_name}
                </Typography>
                <Typography variant="body2" marginTop={1}>
                    {critic.bio}
                </Typography>
            </Stack>
        </Grid>
    )
}

export default CriticsItem