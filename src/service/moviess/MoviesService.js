import serviceApi from "../config/ServiceApi"
import {WS_MOVIE_CRITICS, WS_SEARCH_MOVIE } from "../config/ServiceConfig"

const getListMovieCritics = async (reviewer) => {
    let response = await serviceApi.get(`${WS_MOVIE_CRITICS}/${reviewer}.json`)
    let result = response.data.results

    let listCritics = result.critics.map(
        critic => {
            return {
                display_name: critic.display_name,
                sort_name: critic.sort_name,
                status: critic.status,
                bio: critic.bio,
                seo_name: critic.seo_name
            }
        }
    )

    return {
        listCritics: result.list_name,
        critics: listCritics
    }
}

const SearchCriticsReview = async () => {
    let response = await serviceApi.get(WS_SEARCH_MOVIE)
    let result = response.data.results.map(
        item => {
            return {
                display_title: item.display_title,
                byline: item.byline,
                headline: item.headline,
                summary_short: item.summary_short,
                publication_date: item.publication_date
            }
        }
    )
    
    return result
}



const movieService = {getListMovieCritics, SearchCriticsReview}

export default movieService